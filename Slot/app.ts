﻿var canvas: HTMLCanvasElement;
var ctx: CanvasRenderingContext2D;

var SCREEN_WIDTH: number = 1280;
var SCREEN_HEIGHT: number =  720;

function GameLoop() {
    requestAnimationFrame(GameLoop);
    ctx.fillStyle = "black"
    ctx.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

    userInterface.DisplayUI();
}

window.onload = () => {
    canvas = <HTMLCanvasElement>document.getElementById('cnvs');
    ctx = canvas.getContext("2d");

    player = new Model();
    userInterface = new View(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
    controller = new Controller();

    player.view = userInterface;
    userInterface.controller = controller;
    controller.player = player;

    GameLoop();
}

//class for drawing rectangles
class Rect {
    public x: number = 0;
    public y: number = 0;
    public width: number = 0;
    public height: number = 0;
    public lineWidth: number = 1;
    public color: string = "white"
    constructor(_X: number, _Y: number, _Width: number, _Height: number, _Color: string = "white", _LineWidth: number = 2) {
        this.x = _X;
        this.y = _Y;
        this.width = _Width;
        this.height = _Height;
        this.lineWidth = _LineWidth;
        this.color = _Color;
    }

    public Render = (): void => {
        ctx.save();
        ctx.beginPath();
        ctx.strokeStyle = this.color;
        ctx.rect(this.x, this.y, this.width, this.height);
        ctx.stroke();
        ctx.restore();
    }
}

class UIText {
    public active: boolean = true;
    public x: number = 0;
    public y: number = 0;
    public color: string = "white"
    public text: string = "";
    public fontSize: number = 0;
    public textAlignment: string = "left";
    public textBaseLine: string = "left";

    constructor(x: number, y: number, _FontSize: number, _Text: string, _Color: string, _TextBaseLine = "left", _TextAlignment="left") {
        this.x = x;
        this.y = y;
        this.text = _Text;
        this.fontSize = _FontSize;
        this.color = _Color;
        this.textAlignment = _TextAlignment;
        this.textBaseLine = _TextBaseLine;
    }

    public Render = (): void => {
        if (!this.active)
            return;
        ctx.save();
        ctx.beginPath();
        ctx.textAlign = this.textAlignment;
        ctx.textBaseline = this.textBaseLine;
        ctx.fillStyle = this.color;
        ctx.font = this.fontSize + "px Verdana";
        ctx.fillText(this.text, this.x, this.y);
        ctx.restore();
    }
}

//buttons
class Button {
    public down: boolean = false;
    public x: number;
    public y: number;
    public txt: UIText = new UIText(0, 0, 0, "", "white");
    public startColor: string = "white";
    public pressedColor: string = "red";
    public half_Width: number;
    public half_Height: number;
    public buttonRect: Rect = new Rect(0, 0, 0, 0);

    constructor(x: number, y: number, width: number, height: number, font_Size: number = 32, text: string = "", _StartColor: string = "white", _PressedColor: string = "red") {
        this.x = x;
        this.y = y;
        this.half_Width = width / 2;
        this.half_Height = height / 2;
        this.startColor = _StartColor;
        this.pressedColor = _PressedColor;

        //Rect
        this.buttonRect.x = x - this.half_Width;
        this.buttonRect.y = y - this.half_Height;
        this.buttonRect.width = width;
        this.buttonRect.height = height;
        this.buttonRect.color = _StartColor;

        // UIText
        this.txt.x = x;
        this.txt.y = y;
        this.txt.fontSize = font_Size;
        this.txt.text = text;
        this.txt.textAlignment = "center";
        this.txt.textBaseLine = "middle";
        this.txt.color = _StartColor;

        canvas.addEventListener("mousedown", this.MouseDown, false);
        canvas.addEventListener("mouseup", this.MouseUp, false);
    }

    public Render = (): void => {
        this.txt.x = this.x;
        this.txt.y = this.y;
        this.txt.Render();

        this.buttonRect.x = this.x - this.half_Width;
        this.buttonRect.y = this.y - this.half_Height;

        this.buttonRect.Render();
    }

    public MouseDown = (event: MouseEvent): boolean => {
        var x: number = event.x;
        var y: number = event.y;
        if (x > this.x - this.half_Width && y > this.y - this.half_Height &&
            x < this.x + this.half_Width && y < this.y + this.half_Height) {
            this.down = true;

            this.txt.color = this.pressedColor;
            this.buttonRect.color = this.pressedColor;
            return this.down;
        }
    }

    public MouseUp = (event: MouseEvent): boolean => {
        this.down = false;
        this.txt.color = this.startColor;
        this.buttonRect.color = this.startColor;

        var x: number = event.x;
        var y: number = event.y;
        if (x > this.x - this.half_Width && y > this.y - this.half_Height &&
            x < this.x + this.half_Width && y < this.y + this.half_Height) {
            //Event for when user clicks the button
            return this.down;
        }
    }
}

class Reel {
    public animate: boolean = false;
    public speed: number = 10;
    public x: number;
    public y: number;
    private halfWidth: number = 0;
    private halfHeight: number = 0;

    private rotationCount: number = 3;
    private selectedSymbol: string = "OOO";

    public reelOutline: Rect = new Rect(0, 0, 100, 300);
    private pos1: number = 50;
    private pos2: number = 150;
    private pos3: number = 250;

    public symbol1: UIText = new UIText(0, 0, 35, "777", "red");
    public symbol2: UIText = new UIText(0, 0, 35, "BAR", "white");
    public symbol3: UIText = new UIText(0, 0, 35, "OOO", "blue");

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
        this.halfWidth = this.reelOutline.width / 2;
        this.halfHeight = this.reelOutline.height / 2;

        this.reelOutline.x = x - this.halfWidth;
        this.reelOutline.y = y - this.halfHeight;
        this.pos1 = y - this.reelOutline.height / 3;
        this.pos2 = y;
        this.pos3 = y + this.reelOutline.height / 3;

        this.symbol1.x = x - this.symbol1.fontSize;
        this.symbol1.y = this.pos1;

        this.symbol2.x = x - this.symbol2.fontSize;
        this.symbol2.y = this.pos2;

        this.symbol3.x = x - this.symbol3.fontSize;
        this.symbol3.y = this.pos3;
    }

    public Render = (): void => {
        if (this.animate)
            this.AnimateSymbols();
        this.reelOutline.Render();
        this.symbol1.Render();
        this.symbol2.Render();
        this.symbol3.Render();
    }

    private AnimateSymbols = (): void => {

        if (!this.symbol1.active && this.symbol2.y > this.pos2) {
            this.symbol1.active = true;
            this.rotationCount--;
        }

        if (!this.symbol2.active && this.symbol3.y > this.pos2) {
            this.symbol2.active = true;
        }

        if (!this.symbol3.active && this.symbol1.y > this.pos2) {
            this.symbol3.active = true;
        }


        if (this.symbol1.text == this.selectedSymbol && this.rotationCount <= 0) {
            if (this.symbol1.y == this.pos2) {
                this.symbol3.active = true;
                this.animate = false;
            }
        }
        if (this.symbol2.text == this.selectedSymbol && this.rotationCount <= 0) {
            if (this.symbol2.y == this.pos2) {
                this.symbol1.active = true;
                this.animate = false;
            }
        }
        if (this.symbol3.text == this.selectedSymbol && this.rotationCount <= 0) {
            if (this.symbol3.y == this.pos2) {
                this.symbol2.active = true;
                this.animate = false;
            }
        }


        this.symbol1.y = this.symbol1.y + this.speed;
        this.symbol2.y = this.symbol2.y + this.speed;
        this.symbol3.y = this.symbol3.y + this.speed;

        if (this.symbol1.y > this.y + this.halfHeight) {
            this.symbol1.y = this.pos1;
            this.symbol1.active = false;
        }

        if (this.symbol2.y > this.y + this.halfHeight) {
            this.symbol2.y = this.pos1;
            this.symbol2.active = false;
        }

        if (this.symbol3.y > this.y + this.halfHeight) {
            this.symbol3.y = this.pos1;
            this.symbol3.active = false;
        }
    }

    public SetSymbol = (cS: string): void => {
        this.selectedSymbol = cS;
        this.rotationCount = 3;
        this.animate = true;
    }
}
class BoxText {
    public x: number = 0;
    public y: number = 0;
    public width: number = 0;
    public height: number = 0;
    public txt: UIText = new UIText(0, 0, 0, "", "white");
    public half_Width: number;
    public half_Height: number;
    public rect: Rect = new Rect(0, 0, 0, 0);
    public color: string = "white";

    constructor(x: number, y: number, width: number, height: number, font_Size: number = 32, text: string = "", color = "white") {
        this.x = x;
        this.y = y;
        this.half_Width = width / 2;
        this.half_Height = height / 2;
        this.color = color;
        //Rect
        this.rect.x = x - this.half_Width;
        this.rect.y = y - this.half_Height;
        this.rect.width = width;
        this.rect.height = height;
        this.rect.color = this.color;
        //text
        this.txt.x = x;
        this.txt.y = y;
        this.txt.text = text;
        this.txt.fontSize = font_Size;
        this.txt.textAlignment = "center";
        this.txt.textBaseLine = "middle";
        this.txt.color = color;
    }

    public Render = (): void => {
        this.txt.x = this.x;
        this.txt.y = this.y;
        this.txt.color = this.color;
        this.txt.Render();

        this.rect.x = this.x - this.half_Width;
        this.rect.y = this.y - this.half_Height;
        this.rect.color = this.color;

        this.rect.Render();
    }
}
//user information
class Model {
    public view: View;
    public available: number = 100;
    public won: number = 0;
    private cost: number = 10;
    private bars: string[] = ["777", "BAR", "OOO"];
    private s1Selected: string;
    private s2Selected: string;
    private s3Selected: string;

    constructor(_Available: number = 100) {
        this.available = _Available;
    }
    public Bet = (): void => {
        this.available -= this.cost;

        this.s1Selected = this.bars[Math.floor(Math.random() * this.bars.length)];
        this.s2Selected = this.bars[Math.floor(Math.random() * this.bars.length)];
        this.s3Selected = this.bars[Math.floor(Math.random() * this.bars.length)];
        this.CheckIfWon();
        this.available += this.won;
        this.view.UpdateAvailable(this.available);
        this.view.Spin(this.s1Selected, this.s2Selected, this.s3Selected);
        this.UpdateText();
    }

    private UpdateText = (): void => {
        this.view.UpdateAvailable(this.available);
        this.view.UpdateWon(this.won);
    }

    private CheckIfWon = (): void => {
        if (this.s1Selected == this.bars[0] && this.s2Selected == this.bars[0] && this.s3Selected == this.bars[0]) {
            this.won = 100;
        }
        else if (this.s1Selected == this.bars[1] && this.s2Selected == this.bars[1] && this.s3Selected == this.bars[1]) {
            this.won = 50;
        }
        else if (this.s1Selected == this.bars[2] && this.s2Selected == this.bars[2] && this.s3Selected == this.bars[2]) {
            this.won = 25;
        }
        else {
            this.won = 0;
        }
    }
}

//behind the scenes controls
class Controller {

    public player: Model;

    constructor() {
    }

    public Spin() {
        this.player.Bet();
    }
    
}

//UI
class View {
    private x: number = 0;
    private y: number = 0;
    private width: number = 0;
    private height: number = 0;
    private halfWidth: number = 0;
    private halfHeight: number = 0;
    private rect: Rect = new Rect(0, 0, 0, 0, "white", 25);
    private bTitle: BoxText = new BoxText(0, 0, 480, 100, 32, "Power Slots!", "gold");
    private wonText: BoxText;
    private creditsText: BoxText;

    private btn: Button;
    //private availableCredits: UIText = new UIText(SCREEN_WIDTH / 2 + 50, SCREEN_HEIGHT - 150, 25, "CREDITS: ", "white");
    //private creditsWon: UIText = new UIText(SCREEN_WIDTH / 2 - 150, SCREEN_HEIGHT - 150, 25, "WON: ", "white");

    private reel1: Reel;
    private reel2: Reel;
    private reel3: Reel;

    public controller: Controller;

    constructor(x: number, y: number, width: number = 480, height: number = 720) {

        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.halfWidth = width / 2;
        this.halfHeight = height / 2;

        this.rect.x = this.x - this.halfWidth;
        this.rect.y = this.y - this.halfHeight;
        this.rect.width = width;
        this.rect.height = height;

        this.bTitle.width = width;
        this.bTitle.height = height / 4;

        this.reel1 = new Reel(x-100, y);
        this.reel2 = new Reel(x, y);
        this.reel3 = new Reel(x+100, y);
        //this.reel1.x = x - this.reel1.reelOutline.width;
        //this.reel3.x = x + this.reel3.reelOutline.width;

        this.wonText = new BoxText(this.height, this.width, width / 3, this.height / 10, 20, "Won: 0", "white");
        this.creditsText = new BoxText(this.x, this.height, this.width / 3, this.height / 10, 20, "Credits : 0", "white");
        this.btn = new Button(this.x, this.height, this.width / 3, this.height / 10, 20, "Bet", "white", "red");

        canvas.addEventListener("mousedown", this.BetPressed, false);
        canvas.addEventListener("mouseup", this.BetReleased, false);
    }

    public UpdateAvailable = (newAmount: number): void => {
        this.creditsText.txt.text = "CREDITS: " + newAmount;         
    }

    public DisplayUI = (): void => {
        this.btn.Render();
        this.creditsText.Render();
        this.wonText.txt.Render();
        this.reel1.x = this.x;
        this.reel1.y = this.y;

        this.reel2.x = this.x - this.reel2.reelOutline.width;
        this.reel2.y = this.y;
        this.reel3.x = this.x + this.reel2.reelOutline.width;
        this.reel3.y = this.y;

        this.reel1.Render();
        this.reel2.Render();
        this.reel3.Render();

        this.rect.x = this.x - this.halfWidth;
        this.rect.y = this.y - this.halfHeight;
        this.rect.Render();

        this.bTitle.x = this.x;
        this.bTitle.y = this.y - this.halfHeight + this.bTitle.half_Height;
        this.bTitle.Render();

        this.wonText.x = this.x + this.width / 3;
        this.wonText.y = this.height - this.wonText.half_Height;
        this.wonText.Render();

        this.creditsText.x = this.x - this.width / 3;
        this.creditsText.y = this.height - this.wonText.half_Height;
        this.creditsText.Render();


        this.btn.x = this.x;
        this.btn.y = this.height - this.btn.half_Height;
        this.btn.Render();
    }

    public Spin(r1: string, r2: string, r3: string) {
        this.reel1.SetSymbol(r1);
        this.reel2.SetSymbol(r2);
        this.reel3.SetSymbol(r3);
    }

    public UpdateCredits(currentCredits: number) {
        this.creditsText.txt.text = "CREDITS: " + currentCredits;
    }

    public UpdateWon = (amountWon: number): void => {
        this.wonText.txt.text = "WON: " + amountWon;
    }

    public BetPressed = (event: MouseEvent): void => {
        if (this.reel1.animate || this.reel2.animate || this.reel3.animate)
            return;

        if (this.btn.MouseDown(event)) {
            this.btn.down = true;
            this.btn.txt.color = this.btn.pressedColor;
            this.btn.buttonRect.color = this.btn.pressedColor;
            //tell the controller to make the bet here
            this.controller.Spin();
        }
    }

    public BetReleased = (event: MouseEvent): void => {
        this.btn.down = false;
    }
}

var userInterface: View;
var player: Model;
var controller: Controller;