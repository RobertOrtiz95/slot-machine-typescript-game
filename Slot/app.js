var canvas;
var ctx;
var SCREEN_WIDTH = 1280;
var SCREEN_HEIGHT = 720;
function GameLoop() {
    requestAnimationFrame(GameLoop);
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    userInterface.DisplayUI();
}
window.onload = function () {
    canvas = document.getElementById('cnvs');
    ctx = canvas.getContext("2d");
    player = new Model();
    userInterface = new View(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
    controller = new Controller();
    player.view = userInterface;
    userInterface.controller = controller;
    controller.player = player;
    GameLoop();
};
//class for drawing rectangles
var Rect = (function () {
    function Rect(_X, _Y, _Width, _Height, _Color, _LineWidth) {
        var _this = this;
        if (_Color === void 0) { _Color = "white"; }
        if (_LineWidth === void 0) { _LineWidth = 2; }
        this.x = 0;
        this.y = 0;
        this.width = 0;
        this.height = 0;
        this.lineWidth = 1;
        this.color = "white";
        this.Render = function () {
            ctx.save();
            ctx.beginPath();
            ctx.strokeStyle = _this.color;
            ctx.rect(_this.x, _this.y, _this.width, _this.height);
            ctx.stroke();
            ctx.restore();
        };
        this.x = _X;
        this.y = _Y;
        this.width = _Width;
        this.height = _Height;
        this.lineWidth = _LineWidth;
        this.color = _Color;
    }
    return Rect;
}());
var UIText = (function () {
    function UIText(x, y, _FontSize, _Text, _Color, _TextBaseLine, _TextAlignment) {
        var _this = this;
        if (_TextBaseLine === void 0) { _TextBaseLine = "left"; }
        if (_TextAlignment === void 0) { _TextAlignment = "left"; }
        this.active = true;
        this.x = 0;
        this.y = 0;
        this.color = "white";
        this.text = "";
        this.fontSize = 0;
        this.textAlignment = "left";
        this.textBaseLine = "left";
        this.Render = function () {
            if (!_this.active)
                return;
            ctx.save();
            ctx.beginPath();
            ctx.textAlign = _this.textAlignment;
            ctx.textBaseline = _this.textBaseLine;
            ctx.fillStyle = _this.color;
            ctx.font = _this.fontSize + "px Verdana";
            ctx.fillText(_this.text, _this.x, _this.y);
            ctx.restore();
        };
        this.x = x;
        this.y = y;
        this.text = _Text;
        this.fontSize = _FontSize;
        this.color = _Color;
        this.textAlignment = _TextAlignment;
        this.textBaseLine = _TextBaseLine;
    }
    return UIText;
}());
//buttons
var Button = (function () {
    function Button(x, y, width, height, font_Size, text, _StartColor, _PressedColor) {
        var _this = this;
        if (font_Size === void 0) { font_Size = 32; }
        if (text === void 0) { text = ""; }
        if (_StartColor === void 0) { _StartColor = "white"; }
        if (_PressedColor === void 0) { _PressedColor = "red"; }
        this.down = false;
        this.txt = new UIText(0, 0, 0, "", "white");
        this.startColor = "white";
        this.pressedColor = "red";
        this.buttonRect = new Rect(0, 0, 0, 0);
        this.Render = function () {
            _this.txt.x = _this.x;
            _this.txt.y = _this.y;
            _this.txt.Render();
            _this.buttonRect.x = _this.x - _this.half_Width;
            _this.buttonRect.y = _this.y - _this.half_Height;
            _this.buttonRect.Render();
        };
        this.MouseDown = function (event) {
            var x = event.x;
            var y = event.y;
            if (x > _this.x - _this.half_Width && y > _this.y - _this.half_Height &&
                x < _this.x + _this.half_Width && y < _this.y + _this.half_Height) {
                _this.down = true;
                _this.txt.color = _this.pressedColor;
                _this.buttonRect.color = _this.pressedColor;
                return _this.down;
            }
        };
        this.MouseUp = function (event) {
            _this.down = false;
            _this.txt.color = _this.startColor;
            _this.buttonRect.color = _this.startColor;
            var x = event.x;
            var y = event.y;
            if (x > _this.x - _this.half_Width && y > _this.y - _this.half_Height &&
                x < _this.x + _this.half_Width && y < _this.y + _this.half_Height) {
                //Event for when user clicks the button
                return _this.down;
            }
        };
        this.x = x;
        this.y = y;
        this.half_Width = width / 2;
        this.half_Height = height / 2;
        this.startColor = _StartColor;
        this.pressedColor = _PressedColor;
        //Rect
        this.buttonRect.x = x - this.half_Width;
        this.buttonRect.y = y - this.half_Height;
        this.buttonRect.width = width;
        this.buttonRect.height = height;
        this.buttonRect.color = _StartColor;
        // UIText
        this.txt.x = x;
        this.txt.y = y;
        this.txt.fontSize = font_Size;
        this.txt.text = text;
        this.txt.textAlignment = "center";
        this.txt.textBaseLine = "middle";
        this.txt.color = _StartColor;
        canvas.addEventListener("mousedown", this.MouseDown, false);
        canvas.addEventListener("mouseup", this.MouseUp, false);
    }
    return Button;
}());
var Reel = (function () {
    function Reel(x, y) {
        var _this = this;
        this.animate = false;
        this.speed = 10;
        this.halfWidth = 0;
        this.halfHeight = 0;
        this.rotationCount = 3;
        this.selectedSymbol = "OOO";
        this.reelOutline = new Rect(0, 0, 100, 300);
        this.pos1 = 50;
        this.pos2 = 150;
        this.pos3 = 250;
        this.symbol1 = new UIText(0, 0, 35, "777", "red");
        this.symbol2 = new UIText(0, 0, 35, "BAR", "white");
        this.symbol3 = new UIText(0, 0, 35, "OOO", "blue");
        this.Render = function () {
            if (_this.animate)
                _this.AnimateSymbols();
            _this.reelOutline.Render();
            _this.symbol1.Render();
            _this.symbol2.Render();
            _this.symbol3.Render();
        };
        this.AnimateSymbols = function () {
            if (!_this.symbol1.active && _this.symbol2.y > _this.pos2) {
                _this.symbol1.active = true;
                _this.rotationCount--;
            }
            if (!_this.symbol2.active && _this.symbol3.y > _this.pos2) {
                _this.symbol2.active = true;
            }
            if (!_this.symbol3.active && _this.symbol1.y > _this.pos2) {
                _this.symbol3.active = true;
            }
            if (_this.symbol1.text == _this.selectedSymbol && _this.rotationCount <= 0) {
                if (_this.symbol1.y == _this.pos2) {
                    _this.symbol3.active = true;
                    _this.animate = false;
                }
            }
            if (_this.symbol2.text == _this.selectedSymbol && _this.rotationCount <= 0) {
                if (_this.symbol2.y == _this.pos2) {
                    _this.symbol1.active = true;
                    _this.animate = false;
                }
            }
            if (_this.symbol3.text == _this.selectedSymbol && _this.rotationCount <= 0) {
                if (_this.symbol3.y == _this.pos2) {
                    _this.symbol2.active = true;
                    _this.animate = false;
                }
            }
            _this.symbol1.y = _this.symbol1.y + _this.speed;
            _this.symbol2.y = _this.symbol2.y + _this.speed;
            _this.symbol3.y = _this.symbol3.y + _this.speed;
            if (_this.symbol1.y > _this.y + _this.halfHeight) {
                _this.symbol1.y = _this.pos1;
                _this.symbol1.active = false;
            }
            if (_this.symbol2.y > _this.y + _this.halfHeight) {
                _this.symbol2.y = _this.pos1;
                _this.symbol2.active = false;
            }
            if (_this.symbol3.y > _this.y + _this.halfHeight) {
                _this.symbol3.y = _this.pos1;
                _this.symbol3.active = false;
            }
        };
        this.SetSymbol = function (cS) {
            _this.selectedSymbol = cS;
            _this.rotationCount = 3;
            _this.animate = true;
        };
        this.x = x;
        this.y = y;
        this.halfWidth = this.reelOutline.width / 2;
        this.halfHeight = this.reelOutline.height / 2;
        this.reelOutline.x = x - this.halfWidth;
        this.reelOutline.y = y - this.halfHeight;
        this.pos1 = y - this.reelOutline.height / 3;
        this.pos2 = y;
        this.pos3 = y + this.reelOutline.height / 3;
        this.symbol1.x = x - this.symbol1.fontSize;
        this.symbol1.y = this.pos1;
        this.symbol2.x = x - this.symbol2.fontSize;
        this.symbol2.y = this.pos2;
        this.symbol3.x = x - this.symbol3.fontSize;
        this.symbol3.y = this.pos3;
    }
    return Reel;
}());
var BoxText = (function () {
    function BoxText(x, y, width, height, font_Size, text, color) {
        var _this = this;
        if (font_Size === void 0) { font_Size = 32; }
        if (text === void 0) { text = ""; }
        if (color === void 0) { color = "white"; }
        this.x = 0;
        this.y = 0;
        this.width = 0;
        this.height = 0;
        this.txt = new UIText(0, 0, 0, "", "white");
        this.rect = new Rect(0, 0, 0, 0);
        this.color = "white";
        this.Render = function () {
            _this.txt.x = _this.x;
            _this.txt.y = _this.y;
            _this.txt.color = _this.color;
            _this.txt.Render();
            _this.rect.x = _this.x - _this.half_Width;
            _this.rect.y = _this.y - _this.half_Height;
            _this.rect.color = _this.color;
            _this.rect.Render();
        };
        this.x = x;
        this.y = y;
        this.half_Width = width / 2;
        this.half_Height = height / 2;
        this.color = color;
        //Rect
        this.rect.x = x - this.half_Width;
        this.rect.y = y - this.half_Height;
        this.rect.width = width;
        this.rect.height = height;
        this.rect.color = this.color;
        //text
        this.txt.x = x;
        this.txt.y = y;
        this.txt.text = text;
        this.txt.fontSize = font_Size;
        this.txt.textAlignment = "center";
        this.txt.textBaseLine = "middle";
        this.txt.color = color;
    }
    return BoxText;
}());
//user information
var Model = (function () {
    function Model(_Available) {
        var _this = this;
        if (_Available === void 0) { _Available = 100; }
        this.available = 100;
        this.won = 0;
        this.cost = 10;
        this.bars = ["777", "BAR", "OOO"];
        this.Bet = function () {
            _this.available -= _this.cost;
            _this.s1Selected = _this.bars[Math.floor(Math.random() * _this.bars.length)];
            _this.s2Selected = _this.bars[Math.floor(Math.random() * _this.bars.length)];
            _this.s3Selected = _this.bars[Math.floor(Math.random() * _this.bars.length)];
            _this.CheckIfWon();
            _this.available += _this.won;
            _this.view.UpdateAvailable(_this.available);
            _this.view.Spin(_this.s1Selected, _this.s2Selected, _this.s3Selected);
            _this.UpdateText();
        };
        this.UpdateText = function () {
            _this.view.UpdateAvailable(_this.available);
            _this.view.UpdateWon(_this.won);
        };
        this.CheckIfWon = function () {
            if (_this.s1Selected == _this.bars[0] && _this.s2Selected == _this.bars[0] && _this.s3Selected == _this.bars[0]) {
                _this.won = 100;
            }
            else if (_this.s1Selected == _this.bars[1] && _this.s2Selected == _this.bars[1] && _this.s3Selected == _this.bars[1]) {
                _this.won = 50;
            }
            else if (_this.s1Selected == _this.bars[2] && _this.s2Selected == _this.bars[2] && _this.s3Selected == _this.bars[2]) {
                _this.won = 25;
            }
            else {
                _this.won = 0;
            }
        };
        this.available = _Available;
    }
    return Model;
}());
//behind the scenes controls
var Controller = (function () {
    function Controller() {
    }
    Controller.prototype.Spin = function () {
        this.player.Bet();
    };
    return Controller;
}());
//UI
var View = (function () {
    function View(x, y, width, height) {
        var _this = this;
        if (width === void 0) { width = 480; }
        if (height === void 0) { height = 720; }
        this.x = 0;
        this.y = 0;
        this.width = 0;
        this.height = 0;
        this.halfWidth = 0;
        this.halfHeight = 0;
        this.rect = new Rect(0, 0, 0, 0, "white", 25);
        this.bTitle = new BoxText(0, 0, 480, 100, 32, "Power Slots!", "gold");
        this.UpdateAvailable = function (newAmount) {
            _this.creditsText.txt.text = "CREDITS: " + newAmount;
        };
        this.DisplayUI = function () {
            _this.btn.Render();
            _this.creditsText.Render();
            _this.wonText.txt.Render();
            _this.reel1.x = _this.x;
            _this.reel1.y = _this.y;
            _this.reel2.x = _this.x - _this.reel2.reelOutline.width;
            _this.reel2.y = _this.y;
            _this.reel3.x = _this.x + _this.reel2.reelOutline.width;
            _this.reel3.y = _this.y;
            _this.reel1.Render();
            _this.reel2.Render();
            _this.reel3.Render();
            _this.rect.x = _this.x - _this.halfWidth;
            _this.rect.y = _this.y - _this.halfHeight;
            _this.rect.Render();
            _this.bTitle.x = _this.x;
            _this.bTitle.y = _this.y - _this.halfHeight + _this.bTitle.half_Height;
            _this.bTitle.Render();
            _this.wonText.x = _this.x + _this.width / 3;
            _this.wonText.y = _this.height - _this.wonText.half_Height;
            _this.wonText.Render();
            _this.creditsText.x = _this.x - _this.width / 3;
            _this.creditsText.y = _this.height - _this.wonText.half_Height;
            _this.creditsText.Render();
            _this.btn.x = _this.x;
            _this.btn.y = _this.height - _this.btn.half_Height;
            _this.btn.Render();
        };
        this.UpdateWon = function (amountWon) {
            _this.wonText.txt.text = "WON: " + amountWon;
        };
        this.BetPressed = function (event) {
            if (_this.reel1.animate || _this.reel2.animate || _this.reel3.animate)
                return;
            if (_this.btn.MouseDown(event)) {
                _this.btn.down = true;
                _this.btn.txt.color = _this.btn.pressedColor;
                _this.btn.buttonRect.color = _this.btn.pressedColor;
                //tell the controller to make the bet here
                _this.controller.Spin();
            }
        };
        this.BetReleased = function (event) {
            _this.btn.down = false;
        };
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.halfWidth = width / 2;
        this.halfHeight = height / 2;
        this.rect.x = this.x - this.halfWidth;
        this.rect.y = this.y - this.halfHeight;
        this.rect.width = width;
        this.rect.height = height;
        this.bTitle.width = width;
        this.bTitle.height = height / 4;
        this.reel1 = new Reel(x - 100, y);
        this.reel2 = new Reel(x, y);
        this.reel3 = new Reel(x + 100, y);
        //this.reel1.x = x - this.reel1.reelOutline.width;
        //this.reel3.x = x + this.reel3.reelOutline.width;
        this.wonText = new BoxText(this.height, this.width, width / 3, this.height / 10, 20, "Won: 0", "white");
        this.creditsText = new BoxText(this.x, this.height, this.width / 3, this.height / 10, 20, "Credits : 0", "white");
        this.btn = new Button(this.x, this.height, this.width / 3, this.height / 10, 20, "Bet", "white", "red");
        canvas.addEventListener("mousedown", this.BetPressed, false);
        canvas.addEventListener("mouseup", this.BetReleased, false);
    }
    View.prototype.Spin = function (r1, r2, r3) {
        this.reel1.SetSymbol(r1);
        this.reel2.SetSymbol(r2);
        this.reel3.SetSymbol(r3);
    };
    View.prototype.UpdateCredits = function (currentCredits) {
        this.creditsText.txt.text = "CREDITS: " + currentCredits;
    };
    return View;
}());
var userInterface;
var player;
var controller;
//# sourceMappingURL=app.js.map